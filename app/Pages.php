<?php namespace Avoin;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model {

	public $guarded = ['id'];

    public $table = 'pages';

}
