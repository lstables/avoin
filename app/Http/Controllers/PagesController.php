<?php namespace Avoin\Http\Controllers;

use Avoin\Http\Requests;
use Avoin\Http\Controllers\Controller;
use Avoin\Http\Requests\CreatePageRequest;
use Avoin\Pages;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laracasts\Flash\Flash;

class PagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = Pages::paginate(10);
        return view('pages.index', compact('pages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pages.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePageRequest $request
     * @return Response
     */
	public function store(CreatePageRequest $request)
	{
        $str = array("&","'"," ");
        $rpl = array('-','','');

		$page = new Pages;
        $page->name = $request->input('name');
        $page->title = $request->input('title');
        $page->seo_description = $request->input('seo_description');
        $page->content = $request->input('content');
        $page->active = $request->input('active');
        $page->slug = str_replace($str, $rpl, strtolower($request->input('title')));
        $page->save();

        Flash::success('Page successfully created');
        return redirect('/admin/pages');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Pages::find($id);
        return view('pages.edit', compact('page'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
        $page = Pages::find($id);
        $page->name = $request->input('name');
        $page->title = $request->input('title');
        $page->seo_description = $request->input('seo_description');
        $page->content = $request->input('content');
        $page->update();

        Flash::success('Page successfully updated');
        return redirect('/admin/pages');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $page = Pages::find($id);
        $page->delete();
        Flash::success('Page successfully removed');
        return redirect('/admin/pages');
	}

    public function upload()
    {
        $file = Input::file('file');
        $fileName = $file->getClientOriginalName();

        $file->move(public_path().'/uploads/', $fileName);
        return Response::json(array('filelink' => '/uploads/' . $fileName));
    }

}
