<?php namespace Avoin\Http\Controllers;

use Avoin\Http\Requests\ContactRequest;
use Avoin\Http\Requests\Request;
use Avoin\Pages;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;

class WelcomeController extends Controller {


	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $pages = Pages::where('active', '=', 1)->get();
		return view('main', compact('pages'));
	}

    public function show($page)
    {
        $pageContent = Pages::where('slug', $page)->first();
        $pages = Pages::where('active', '=', 1)->get();
        return view('main', compact('pageContent', 'pages'));
    }

    public function contact(ContactRequest $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $comments = $request->input('comments');

         $data = array(
             'name'      => $name,
             'email'     => $email,
             'comments'  => $comments
         );
         // Send the email
         Mail::send('emails/contact', $data, function( $message ) use ($data)
         {
             $message->to('info@avoinmaasurveys.com')
                     ->from($data['email'])
                     ->subject('Avoin Maa Surveys - Feedback Form Submission');
         });
        Flash::success('Thank You! We have received your email.');
        return redirect()->back();
    }

}
