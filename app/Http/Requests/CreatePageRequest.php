<?php namespace Avoin\Http\Requests;

use Avoin\Http\Requests\Request;

class CreatePageRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required',
            'title' => 'required|max:160|min:2',
            'seo_description' => 'required|max:160|min:2',
		];
	}

}
