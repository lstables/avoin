<?php

/* Frontend */
Route::get('/', 'WelcomeController@index');
Route::post('contact','WelcomeController@contact');

Route::any('/{page}','WelcomeController@show');


Route::group(['prefix' => 'admin'], function() {
    /* Dashboard */
    Route::get('dashboard', 'HomeController@index');


    Route::post('pages/update/{id}','PagesController@update');
    Route::get('pages/delete/{id}','PagesController@destroy');
    Route::resource('pages', 'PagesController');
});

/* Auth */
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/* Uploads */
Route::post('redactorUpload', function()
{
    $file = Request::file('file');
    $fileName = $file->getClientOriginalName();

    $file->move(public_path().'/uploads/', $fileName);
    return Response::json(array('filelink' => '/uploads/' . $fileName));
});
