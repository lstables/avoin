<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Avoin Maa Surveys</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- stylesheets -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- javascript/jQuery -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="/js/wow.js"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
    <![endif]-->
    <script>
        new WOW().init();
    </script>

</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">Avoin Maa Surveys</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                @foreach($pages as $pg)
                    <li><a href="/{{ $pg->slug }}">{{ ucwords($pg->slug) }}</a></li>
                @endforeach
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Tel: 01226 292377 or 07969 360287</a></li>
            </ul>
        </div>
    </div>
</nav>
@if (Request::is('/'))
    <div class="container content">
        <div class="row">
            <div class="col-lg-11">
                @yield('content')
            </div>
        </div>
    </div>
    <hr />
    </div><!-- container -->

    <div class="divider-image img-responsive">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3>SERVICE AVOIDANCE</h3>
                    <p>
                        A must when excavating the ground and avoiding dangers from buried services
                    </p>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4 center">
                    <h3>DESKTOP SEARCHES</h3>
                    <p> We can provide all network provider
                        Guidance plans to aid surveys and developments</p>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <h3>TOPOGRAPHICAL SURVEYS</h3>
                    <p>
                        Provided to produce accurate scale drawings, elevations, internals and XYZ Co-ordinates of boreholes etc
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <h3>UTILITY MAPPING</h3>
                    <p>Detection and scaled mapping of buried services produced in CAD to aid developments
                    </p>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <h3>CCTV</h3>
                    <p>Drainage surveys producing plans and reports with footage and connectivity of drainage systems
                    </p>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="padding-left: 16px; padding-bottom: 2px; border-bottom: 1px solid #efefef;">Sponsoring our community</h4>
                <div class="col-md-2">
                    <h5>Barnsley School Boys FA</h5>
                    <!--<img src="barnsley school boys FA Sponsor.jpg" class="img-responsive img-rounded">-->
                </div>
                <div class="col-md-2">
                    <h5>Dodworth St John’s Primary School Football Team</h5>
                    <!--<img src="dodworth-school.png" class="img-responsive img-rounded">-->
                </div>
                <div class="col-md-2">
                    <h5>Bobby Hassell's Testimonial</h5>
                    <!--<img src="bobby-hassell.jpg" class="img-responsive img-rounded">-->
                </div>
                <div class="col-md-2">
                    <h5>Barnsley Footballer's Official Shirts</h5>
                    <!--<img src="" class="img-responsive img-rounded">-->
                </div>
                <div class="col-md-2">
                    <h5>Junior Tykes Football Team</h5>
                </div>
            </div>
        </div>
    </div>
    <hr />
@else
    @yield('content')
@endif
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <small class="pull-left" style="padding-top: 30px;">
                        &copy; Avoin Maa Surveys Ltd. All rights reserved.
                        <br />
                        Company No. 7065044 | info@avoinmaasurveys.com | www.avoinmaasurveys.com
                    </small>
                </div>
                <div class="col-md-6">
                    <small class="pull-right">Created by <a href="http://www.infinety.co.uk" title="Web Design Sheffield - Infinety" target="_blank" rel="follow">Web Design Sheffield</a> Infinety</small>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>