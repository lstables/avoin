@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    Hi {!! Auth::user()->name !!}<br />
                    You are now logged in!
                </div>
            </div>
        </div>
    </div>
@endsection