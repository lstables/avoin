@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pages
                    <div class="pull-right" style="margin-top: -5px;"><a href="/admin/pages/create" class="btn btn-sm btn-primary">Create Page</a></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Page Title</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach ($pages as $page)
                                <tr>
                                    <td>{!! $page->name !!}</td>
                                    <td>{!! $page->title !!}</td>
                                    <td>@if ($page->active == 1) <span class="label label-success">Live</span> @else <span class="label label-danger">Draft</span> @endif</td>
                                    <td>
                                        <a href="/admin/pages/{!! $page->id !!}/edit" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="/admin/pages/delete/{!! $page->id !!}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to remove this?')">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection