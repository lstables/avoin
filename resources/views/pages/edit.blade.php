@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editing ({!! $page->name!!})</div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/pages/update/' . $page->id, 'class' => 'form-horizontal']) !!}

                    <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                        <div class="col-sm-2">Name</div>
                        <div class="col-lg-5">
                            <input type="text" name="name" class="form-control" value="{!! $page->name !!}">
                            <div class="help-block">Name of the page, for your reference only.</div>
                        </div>
                        <div class="col-lg-4">{!! $errors->first('name', '<span class="help-block">:message</span>') !!}</div>
                    </div>

                    <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                        <div class="col-sm-2">Title</div>
                        <div class="col-lg-5">
                            <input type="text" name="title" class="form-control" value="{!! $page->title !!}">
                            <div class="help-block">Title of the page, to help with Google search.</div>
                        </div>
                        <div class="col-lg-4">{!! $errors->first('title', '<span class="help-block">:message</span>') !!}</div>
                    </div>

                    <div class="form-group {!! $errors->has('seo_description') ? 'has-error' : '' !!}">
                        <div class="col-sm-2">SEO Description</div>
                        <div class="col-lg-5">
                            <input type="text" name="seo_description" class="form-control" value="{!! $page->seo_description !!}">
                        </div>
                        <div class="col-lg-4">{!! $errors->first('seo_description', '<span class="help-block">:message</span>') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">Content</div>
                        <div class="col-lg-9">
                            <textarea name="content" class="form-control" rows="20" id="content">{!! $page->content !!}</textarea>
                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<div class="col-sm-2">Status</div>--}}
                        {{--<div class="col-lg-5">--}}
                            {{--<select name="active" class="form-control">--}}
                                {{--<option value="1">Active</option>--}}
                                {{--<option value="0">Draft</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <div class="col-lg-3 col-lg-offset-2">
                            <button class="btn btn-success">Save</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection