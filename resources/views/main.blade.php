<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    @if(Request::is('/'))
    <title>Utility Mapping, Surveying - Avoin Maa Surveys</title>
    <meta name="description" content="Avoin Maa Surveys offers expert on-site services in the location and mapping of underground utilities, priding ourselves on providing and delivering accurate and detailed information to our all our clients.">
    @else
    <title>Avoin Maa Surveys - @if ($pageContent) {!! $pageContent->title !!} @endif</title>
    <meta name="description" content="@if ($pageContent) {!! $pageContent->seo_description !!} @endif">
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-site-verification" content="m8WuxRZHQFlWdHBpTNxkUy4w9279CoNTVr0Bldm6wc4" />

    <!-- stylesheets -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- javascript/jQuery -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="/js/wow.js"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
    <![endif]-->
    <script>
        new WOW().init();
    </script>
    <script>
        window.setTimeout(function() {
            $(".alert").delay(600, 0).slideUp(190, function(){
              $(this).remove();
            });
         }, 3000);
     </script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">Avoin Maa Surveys</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                @foreach($pages as $pg)
                    <li><a href="/{{ $pg->slug }}">{{ ucwords($pg->slug) }}</a></li>
                @endforeach
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Tel: 01226 292377 or 07969 360287</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container content">
    <div class="row">
        <div class="col-md-6">
            <img src="/images/logo.png" class="img-responsive"><br />
            @if (Request::is('/'))
            <div class="col-md-10">
                <p>
                    Avoin Maa Surveys are a utility mapping company offering the best available experience combined with the best available technology, We supply environmental, civil and highway sectors with quality utility surveys.<br /><br />

                    Our site experienced surveyors are able to considerably reduce the risk of loss to finance and to that of employees safety when excavating the ground. Full utility mapping can also be utilised to aid the design of civil projects.
                </p>
                <h3 class="orange">Why Choose Us</h3>
                <ul class="list-unstyled">
                    <li class="wow slideInLeft">Dedication</li>
                    <li class="wow slideInRight">Innovation</li>
                    <li class="wow bounceInUp">Methodical Thinking</li>
                    <li class="wow bounceInDown">Inspirational</li>
                    <li class="wow bounceInRight">Excellence as standard</li>
                </ul>
            </div>
            @else
            <div class="col-md-10">
                <h3>{!! $pageContent->title !!}</h3>
                {!! $pageContent->content !!}
            </div>

                @if (Request::is('contact'))
                    <div class="col-md-10">
                    <hr />
                        @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{!! Session::get('flash_notification.level') !!}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! Session::get('flash_notification.message') !!}
                            </div>
                        @endif
                    {!! Form::open(['url' => 'contact', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <div class="col-sm-3">Your Name</div>
                            <div class="col-lg-9 {!! $errors->has('name') ? 'has-error' : '' !!}">
                                <input type="text" name="name" class="form-control" value="">
                                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">Email</div>
                            <div class="col-lg-9 {!! $errors->has('email') ? 'has-error' : '' !!}">
                                <input type="text" name="email" class="form-control" value="">
                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">Comments</div>
                            <div class="col-lg-9">
                                <textarea name="comments" rows="7" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-lg-3">
                             <button class="btn btn-success">Send</button>
                          </div>
                        </div>
                    {!! Form::close() !!}
                    </div>
                @endif
            @endif
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img class="first-slide img-circle" src="http://www.partneresi.com/images/2886181Large%20-%20surveyor%20at%20work%20for%20web.jpg" alt="First slide">
                            </div>

                            <div class="item">
                                <img class="img-circle" src="/images/utility mapping Lotus F1.jpg">
                            </div>

                            <div class="item">
                                <img class="img-circle" src="/images/proposed bus stop scheme.jpg">
                            </div>

                        </div>
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="fa fa-chevron-circle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="fa fa-chevron-circle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- /.carousel -->
                    <br />
                    <div class="col-md-12">
                        <h4 class="orange center">Statement of Intent</h4>
                        To provide an unrivaled service within the industry offering a new level of dedication and quality to the Environmental, Geotechnical and Civil Sectors
                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr />
</div><!-- container -->

<div class="divider-image img-responsive">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="bg"><h3>SERVICE AVOIDANCE</h3><hr class="bgline"></div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4 center">
                <div class="bg"><h3>DESKTOP SEARCHES</h3><hr class="bgline"></div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="bg"><h3>TOPOGRAPHICAL SURVEYS</h3><hr class="bgline"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
                <div class="bg"><h3>UTILITY MAPPING</h3><hr class="bgline"></div>
            </div>
            <div class="col-md-4 center"></div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="bg"><h3>CCTV</h3><hr class="bgline"></div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>
<br /><br />
<div class="col-md-1"><br /><br /><br /><br /><br /><br /></div>
<div class="col-md-1"><br /><br /><br /><br /><br /><br /></div>
<hr />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 style="padding-left: 16px; padding-bottom: 2px; border-bottom: 1px solid #efefef;">Sponsoring our community</h4>
            <div class="col-md-2">
                <h5>Barnsley School Boys FA</h5>
                <img src="images/barnsley school boys FA Sponsor.jpg" class="img-responsive img-rounded">
            </div>
            <div class="col-md-2">
                <h5>Dodworth St John’s Primary School Football Team</h5>
                <img src="images/dodworth-school.png" class="img-responsive img-rounded">
            </div>
            <div class="col-md-2">
                <h5>Bobby Hassell's Testimonial</h5>
                <img src="images/bobby-hassell.jpg" class="img-responsive img-rounded">
            </div>
            <div class="col-md-2">
                <h5>Barnsley Footballer's Official Shirts</h5>
                <img src="" class="img-responsive img-rounded">
            </div>
            <div class="col-md-2">
                <h5>Junior Tykes Football Team</h5>
            </div>
        </div>
    </div>
</div>
<hr />

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <small class="pull-left" style="padding-top: 30px;">
                        &copy; Avoin Maa Surveys Ltd. All rights reserved.
                        <br />
                        Company No. 7065044 | info@avoinmaasurveys.com | www.avoinmaasurveys.com
                    </small>
                </div>
                <div class="col-md-6">
                    <small class="pull-right">Created by <a href="http://www.infinety.co.uk" title="Web Design Sheffield - Infinety" target="_blank" rel="follow">Web Design Sheffield</a> Infinety</small>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>