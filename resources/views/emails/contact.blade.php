<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- stylesheets -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            Hi Scott,<br />

            You have received a website form submission from {{ $name }}.<br /><br />

            Here's the details:<br />

            Name: {{ $name }}<br />
            Email: {{ $email }}<br />
            Comments: {{ $comments }}<br />
        </div>
    </div>
</div>


</body>
</html>